package id.ac.ub.papb.recycler1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        Bundle hasil = getIntent().getExtras();
        String nim = hasil.getString("nim");
        String nama = hasil.getString("nama");
        TextView nimtv = findViewById(R.id.tvNim2);
        TextView namatv = findViewById(R.id.tvNama2);
        nimtv.setText(nim);
        namatv.setText(nama);

    }
}