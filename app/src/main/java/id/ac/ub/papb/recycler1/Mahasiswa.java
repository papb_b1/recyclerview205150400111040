package id.ac.ub.papb.recycler1;

import java.util.ArrayList;

public class Mahasiswa {
    public String nim;
    public String nama;

    static public ArrayList<Mahasiswa> listMhs = new ArrayList<Mahasiswa>();

    public Mahasiswa(String nim, String nama) {
        this.nim = nim;
        this.nama = nama;
        Mahasiswa.listMhs.add(this);
    }

    public Mahasiswa() {
        Mahasiswa.listMhs.add(this);

    }
}
